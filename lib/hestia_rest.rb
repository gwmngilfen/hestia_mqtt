require 'rest-client'
require 'json'
require 'time'

# This model represents the API calls that can be made to HestiaPi's web
# interface, and the statuses it can return
#
class HestiaRest
  attr_accessor :uri, :log

  def initialize(uri, log)
    @uri = uri
    @log = log
  end

  def logger(msg = '')
    open(log, 'a') { |f| f.puts "[#{Time.now.strftime('%F %T')}] - " + msg }
  end

  def status
    JSON.parse RestClient.get("#{@uri}/api/heating/status")
  end

  def calculate_heat_boost
    # Hestia API returns milliseconds since Epoch as the time the boost will end
    if status['HeatingBoost'] == 'ON'
      ((Time.strptime(st['HeatingBoostTime'], '%Q') - Time.now) / 60).floor
    else
      0
    end
  end

  def calculate_water_boost
    # Hestia API returns milliseconds since Epoch as the time the boost will end
    if st['WaterBoost'] == 'ON'
      ((Time.strptime(st['WaterBoostTime'], '%Q') - Time.now) / 60).floor
    else
      0
    end
  end

  def boost(time, method)
    # method is "Heating" or "Water"
    time = time.to_s # must be a string

    # Hestia only toggles, so send an extra POST to reset it if it's already ON
    if status["#{method}Boost"] == 'ON'
      url = "#{@uri}/api/heating/boost/toggle/#{method.downcase}/time/#{time}"
      RestClient.post(url, time)
    end

    # Set the boost
    url = "#{@uri}/api/heating/boost/toggle/#{method.downcase}/time/#{time}"
    response = RestClient.post(url, time)

    logger "#{method} boost set to #{time} [code: #{response.code}]"
  end

  def boost_heat(time = '120')
    boost(time, 'Heating')
  end

  def boost_water(time = '30')
    boost(time, 'Water')
  end

  def change_temp(method)
    # method is "inc" or "dec"
    url = "#{@uri}/api/heating/boost/toggle/#{method}desiredtemp"
    RestClient.post(url, '')
  end

  def loop_temp(temp)
    temp = temp.to_f
    current_setpoint = status['DesiredTemp'].to_f
    until (current_setpoint - setpoint).abs < 0.5
      if current_setpoint < temp
        change_temp('inc')
      else
        change_temp('dec')
      end
      current_setpoint = status['DesiredTemp'].to_f
    end
  end

  def change_setpoint(temp)
    case temp
    when '+'
      change_temp('inc')
    when '-'
      change_temp('dec')
    else
      loop_temp(temp)
    end
    logger "Setpoint now #{status['DesiredTemp']}"
  end
end
