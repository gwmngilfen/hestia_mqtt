require 'mqtt'
require 'uri'
require 'date'

# Require Hestia Bindings
require File.expand_path('hestia_rest.rb', File.dirname(__FILE__))

# Create a Hestia REST object
$hestia = HestiaRest.new($opts['hestia_url'], $opts['log'])

# Create a hash with the connection parameters from the URL
def conn_opts
  uri = URI.parse $opts['mqtt_url']
  {
    remote_host: uri.host,
    remote_port: uri.port,
    username: uri.user,
    password: uri.password
  }
end

def logger(msg = '')
  open($opts['log'], 'a') do |f|
    f.puts "[#{Time.now.strftime('%F %T')}] - " + msg
  end
end

def channel_prefix
  $opts['channel_prefix']
end

# Subscribe to some channels for controlling HestiaPi
def control_thread
  t = Thread.new do
    logger 'Starting receiver'
    begin
      MQTT::Client.connect(conn_opts) do |c|
        # These roughly correspond to the WebUI AJAX calls for the buttons
        c.subscribe("#{channel_prefix}/boost_heat")
        c.subscribe("#{channel_prefix}/boost_water")
        c.subscribe("#{channel_prefix}/change_setpoint")
        # The block will be called when new messages arrive to the topic
        c.get do |topic, message|
          $hestia.send(topic.split('/').last, message)
        end
      end
    rescue Errno::ECONNREFUSED => e
      logger "[#{e.message}] Receiver cannot connect, sleeping 10s"
      sleep 10
      logger 'Restarting receiver'
      retry
    end
  end
  t
end

def publish_temp(c)
  # Raw format is 19150 so divide by 1k to get 19.15
  temp_c = `/opt/boilercontrol/scripts/gettemperature.sh`.strip.to_f / 1000
  # set retain so that we always have a temp to work on
  c.publish("#{channel_prefix}/status/temp_c", temp_c, true)
end

def publish_api(c)
  st      = $hestia.status
  channel = "#{channel_prefix}/status"

  # Time remaining on boosts has to be calculated
  boost_heat_time  = $hestia.calculate_heat_boost
  boost_water_time = $hestia.calculate_water_boost

  c.publish("#{channel}/heat",             st['Heating'],      true)
  c.publish("#{channel}/boost_heat",       st['HeatingBoost'], true)
  c.publish("#{channel}/boost_heat_time",  boost_heat_time,    true)
  c.publish("#{channel}/water",            st['Water'],        true)
  c.publish("#{channel}/boost_water",      st['WaterBoost'],   true)
  c.publish("#{channel}/boost_water_time", boost_water_time,   true)
  c.publish("#{channel}/setpoint",         st['DesiredTemp'],  true)
end

# Publish current temperature from HestiaPi
def publish_thread
  logger 'Starting publisher'
  MQTT::Client.connect(conn_opts) do |c|
    loop do
      publish_temp(c)
      publish_api(c)
      sleep 30
    end
  end
rescue Errno::ECONNREFUSED => e
  logger "[#{e.message}] Publisher cannot connect, sleeping 10s"
  sleep 10
  retry
end
