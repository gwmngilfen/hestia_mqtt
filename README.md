# Hestia MQTT Bindings

This is a small Ruby application which integreates with the
[HestiaPi](http://www.hestipi.com) Open Source Thermostat to provide MQTT
interaction. The intended use is for reporting & control in
[OpenHAB](http://www.openhab.org).

## Setup

Setup requires a few steps:

* Install bundler on the Pi (`apt-get install bundler`)
* Install git on the Pi (`apt-get install git-core`)
* Clone this repo (`cd /opt ; git clone https://gitlab.com/gwmngilfen/hestia_mqtt.git`)
* Install Ruby dependencies (`cd /opt/hestia_mqtt ; bundle install --path vendor`)
* Copy the init script (`cp extra/hestia_mqtt.init.d /etc/init.d/hestia_mqtt`)
* Edit the config file to suit (`vi config/settings.yaml`)
* Start the app (`/etc/init.d/hestia_mqtt start`)

You should now have `hestia_mqtt` running - check with `ps aux`!

## Configuration

The configuration file four settings you can alter

### mqtt_url (default: 'mqtt://localhost:1883')

This is the host, port, etc, of the MQTT broker. This is where Hestia_MQTT will
publish information and listen for commands.  It defaults to localhost on the
standard MQTT port.

### hestia_url (default: 'http://pi:hestia@localhost:80')

This is the login details for the Hestia UI - we need this to make API calls to
Hestia to retrieve status information and trigger actions. It defaults to the
default credentials that Hestia ships with.

### channel_prefix (default: 'hestia')

This is the prefix that will be added to all the channels Hestia_MQTT
broadcasts on/listens to. It defaults to 'hestia', and within that channel, the
following subchannels are created:

#### MQTT channels:

* Control:
  * elysium/hestia/boost_heat - send an Integer number of minutes to boost heat for
  * elysium/hestia/boost_water - send an Integer number of minutes to boost water for
  * elysium/hestia/set_temp - send a Float new setpoint temperature

* Publishing:
  * elysium/hestia/status/temp_c          - Current Temp Celsius
  * elysium/hestia/status/setpoint        - Current Temperature Setpoint
  * elysium/hestia/status/heat            - Heating State
  * elysium/hestia/status/boost_heat      - Heating Boost State
  * elysium/hestia/status/boost_heat_time - Remaining Heating Boost Time
  * elysium/hestia/status/water           - Water State
  * elysium/hestia/status/boost_water     - Water Boost State
  * elysium/hestia/boost_water_time       - Remaining Water Boost Time

### log (default: './hestia_mqtt.log')

Simply what file to log to. It's recommended to change this to something in `/var/log`

# Copyright

Copyright (c) 2017 Greg Sutcliffe <greg@emeraldreverie.org>

# License

See [[LICENSE]] file.
