require 'simplecov'
SimpleCov.start do
  add_filter '/vendor/'
end

$LOAD_PATH << File.join(File.dirname(__FILE__), '..', 'lib')

# add `binding.pry` wherever you need to debug
# require 'pry' if ENV['APP_ENV'] == 'debug'
