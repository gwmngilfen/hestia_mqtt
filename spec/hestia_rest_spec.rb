require 'spec_helper'
require 'hestia_rest'

describe 'HestiaRest' do
  it 'url is set' do
    hestia = HestiaRest.new('url', 'log') # Given
    expect(hestia.uri).to eq('url') # Then
  end

  it 'log is set' do
    hestia = HestiaRest.new('url', 'log') # Given
    expect(hestia.log).to eq('log')       # Then
  end
end
